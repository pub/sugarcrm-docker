# SugarCRM Docker setup

## Prerequesites

- Need to have both `docker` and `docker-compose` installed.
- Sugar install ZIP file
- Sugar licence key

## Semi-automatic setup

1. Copy `.env.example` to `.env`

2. Run setup script
   `./setup.sh /path/to/sugar-install.zip`

2. Go to [http://localhost](http://localhost) and follow Sugar install instructions

## Manual setup

1. Copy `.env.example` to `.env`

2. Build sugarphp image
   `docker build -t sugarphp:7.4 sugarphp/`

3. Create all required directories
   `mkdir elastic sugar mysql`

4. Bring up docker containers
   `USERID=$(id -u) GROUPID=$(id -g) docker-compose up -d`

5. Extract files
    ```
    cd sugar/
    unzip /path/to/SugarEnt-Full-11.0.0.zip
    mv SugarEnt-Full-11.0.0/{*,*} .
    rmdir SugarEnt-Full-11.0.0
    ```

5. Go to [http://localhost](http://localhost) and follow Sugar install instructions


## Default service credentials

### MySQL
- __Hostname:__ mysql
- __Username:__ root
- __Password__: toor

### Elasticsearch
- __Hostname:__ elastic
- __Port:__ 9200 (default)


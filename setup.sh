#! /bin/bash
set -e # Exit on failure
set -x

ZIPPATH=$1

if [ ! -f "$ZIPPATH" ]; then
	echo "File not found: $ZIPPATH"
	exit 255
fi

# Build PHP image
docker build -t sugarphp:7.4 sugarphp/

# Create required directories
mkdir -p elastic sugar mysql

# Extract files
cd sugar/
unzip -qq "$ZIPPATH"
mv *-Full-*/{.htaccess,*} .
rmdir *-Full-*/
cd ..

# Bring up docker container
USERID=$(id -u) GROUPID=$(id -g) docker-compose up -d

